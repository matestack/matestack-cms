MatestackUiCore.Vue.component('matestack-ui-cms-asset-upload', {
  mixins: [MatestackUiCore.componentMixin],
  data: function data() {
    return {
      file: ""
    };
  },
  methods: {
    handleFileUpload(){
      this.file = this.$refs.file.files[0];
      this.submitFile()
    },
    submitFile(){
      var self = this;
      var token = document.getElementsByName("csrf-token")[0].getAttribute('content');
      var formData = new FormData();
      formData.append('authenticity_token', token);
      formData.append(this.componentConfig["key"], this.file);
      MatestackUiCore.axios.post(this.componentConfig["upload_path"],
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
      ).then(function(){
        MatestackUiCore.matestackEventHub.$emit("action_success")
        self.file = ""
        location.reload();
      })
      .catch(function(){
        MatestackUiCore.matestackEventHub.$emit("action_error")
      });
    }
  }
});

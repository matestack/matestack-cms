class AddActiveStateToContentItems < ActiveRecord::Migration[5.2]
  def change
    add_column :matestack_cms_content_items, :active, :boolean, default: true
  end
end

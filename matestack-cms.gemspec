$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "matestack/cms/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "matestack-cms"
  spec.version     = Matestack::Cms::VERSION
  spec.authors     = ["Jonas Jabari"]
  spec.email       = ["jonas@matestack.io"]
  spec.homepage    = "https://matestack.io"
  spec.summary     = "Generic CMS for Rails"
  spec.description = "Generic CMS for Rails using ActiveStorage for media uploads and Redis for caching"
  spec.license     = "private"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.4", ">= 5.2.4.1"
  spec.add_dependency "mobility", "~> 0.8.9"
  spec.add_dependency "redis"
end

# desc "Explaining what the task does"
# task :matestack_cms do
#   # Task goes here
# end

namespace :matestack_cms do

  desc "Delte all content and assets created through the cms"
  task destroy: :environment do
    redis = Redis.new(host: "redis")
    redis.keys.each {|k| redis.del k }
    Matestack::Cms::ContentItem.all.each { |ci| ci.destroy }
    ActiveRecord::Base.connection.reset_pk_sequence!('content_items')
  end

  desc "Reset the database with content and assets specified in yml files stored in db/cms/seeds"
  task reset: :environment do
    redis = Redis.new(host: "redis")
    redis.keys.each {|k| redis.del k }
    Matestack::Cms::ContentItem.all.each { |ci| ci.destroy }
    ActiveRecord::Base.connection.reset_pk_sequence!('content_items')
    Matestack::Cms::ContentItem.seed
  end

  desc "Seed the database with content and assets specified in yml files stored in db/cms/seeds"
  task seed: :environment do
    Matestack::Cms::ContentItem.seed
  end

  desc "Extend the database with content and assets specified in yml files stored in db/cms/seeds if not present in database"
  task extend: :environment do
    Matestack::Cms::ContentItem.extend
  end

end

module Matestack
  module Cms
    class Engine < ::Rails::Engine
      isolate_namespace Matestack::Cms

    end
  end
end

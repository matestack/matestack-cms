# Matestack::Cms

`matestack-cms` is Rails engine for content editing built on top of [mobility](https://github.com/shioyama/mobility)
and [ActiveStorage](https://edgeguides.rubyonrails.org/active_storage_overview.html)

It can be used for single- and multilanguange content/asset editing.
and supports a developer friendly workflow alongside easy content editing via an Admin UI.

Currently only Postgres is supported.

**Comparison to I18n**

Imagine a simple I18n localization file like:

`app/config/locales/en.yml`

```yml
en:
  hello: "Hello world"
  foo:
    bar: "baz!"
```

`app/config/locales/de.yml`

```yml
de:
  hello: "Hallo Welt!"
  foo:
    bar: "baz!"
```

You then access the content like:

```ruby
I18n.locale # :en
I18n.t('hello') # Hello world!
I18n.t('foo') # { bar: "baz!" }
I18n.t('foo.bar') # baz!
```

Depending on the current `I18n.locale`, you get the correct content. Great!
Using this approach, you can setup the (multilanguange) content of your application easily.

**But what about content editing via an UI?**

`matestack-cms` wants to follow this developer friendly approach of defining content but at the same time
enable content editing via an Admin UI. Therefore the content is stored in the database (and redis cache)
but can be migrated using well known (localized) `yml` files.

These `yml` files parsed via a rake task which properly creates the database
entries for you. These database entries are then editable via the Admin UI.
The content is structured as atomic `ContentItem` nodes. Each node may define text based
values (one for each locale) OR hold a collection of child nodes.

_Example_

```yml
en:
  hello: "Hello world"
  foo:
    bar: "baz!"
```

```yml
de:
  hello: "Hallo Welt!"
  foo:
    bar: "baz!"
```

If `matestack-cms` parses these two files via `rake matestack_cms:seed`,
it will generate multiple `ContentItem` nodes:

```ruby
Matestack::Cms::ContentItem id: 1, key: "hello", parent_content_item_id: nil
# value_en returns "Hello world", value_de returns "Hallo Welt"
Matestack::Cms::ContentItem id: 2, key: "foo", parent_content_item_id: nil
# value_en returns nil, value_de returns nil
Matestack::Cms::ContentItem id: 3, key: "foo", parent_content_item_id: 2
# value_en returns "baz!", value_de returns "baz!"
```

`value_de` and `value_en` are methods returning the proper value depending on the current locale.
These methods come from the `mobility` gem. The actual values are stored in a separate
mobility table in the applications database.

The content is then accessible via the `cms` helper method:

```ruby
I18n.locale # :en
cms('hello') # Hello world!
cms('foo') # { bar: "baz!" }
cms('foo.bar') # baz!
```

or without the helper method directly like:

```ruby
Matestack::Cms::ContentItem.resolve_content('hello', :de) # Hallo Welt
```

If you then add some new content to the the `yml` files like:

`db/cms/seeds/content_items/en.yml`

```yml
en:
  hello: "Hello world"
  foo:
    bar: "baz!"
    some_new_content: "foo!" # new content
```

`db/cms/seeds/content_items/de.yml`

```yml
de:
  hello: "Hallo Welt!"
  foo:
    bar: "baz!"
    some_new_content: "foo!" # new content
```

and call `rake matestack_cms:extend`, `matestack-cms` will add a new `ContentItem` node
**without** touching the existing ones:

```ruby
Matestack::Cms::ContentItem id: 1, key: "hello", parent_content_item_id: nil
# value_en returns "Hello world", value_de returns "Hallo Welt"
Matestack::Cms::ContentItem id: 2, key: "foo", parent_content_item_id: nil
# value_en returns nil, value_de returns nil
Matestack::Cms::ContentItem id: 3, key: "foo", parent_content_item_id: 2
# value_en returns "baz!", value_de returns "baz!"
Matestack::Cms::ContentItem id: 4, key: "some_new_content", parent_content_item_id: 2
# value_en returns "foo!", value_de returns "foo!"
```

In order to enable content management for non programmers,
`matestack-cms` offers `ContentItem` CRUD functionality within the prebuilt Admin UI pages

To drop your current CMS content and re-create from your seeds, run `rake matestack_cms:reset`

**What about Assets?**

When creating a Rails web application, you often need to integrate images and files.
And like text based content, these assets should often be editable via an Admin UI.
`matestack-cms` therefore supports easy asset handling for developers while at the same time
offering an asset management via an Admin UI.

We can extend the simple `yml` file from the last example as:

`db/cms/seeds/content_items/en.yml`

```yml
en:
  hello: "Hello world"
  foo:
    bar: "baz!"
    some_new_content: "foo!"
    my_image: "asset:some/path/some_image.png" # tell matestack-cms that this one is an asset by prefixing the path with 'asset:'
```

When calling `rake matestack_cms:seed` or `rake matestack_cms:extend`, `matestack-cms`
will create a `ContentItem` with the key `my_image`, look for
`db/cms/assets/some/path/some_image.png` and attach this image to the `ContentItem` instance
through `ActiveStorage`

You can get the path to that stored image like:

```ruby
cms('foo.my_image') # ActiveStorage processed path (depends on your ActiveStorage configuration)
```

**Note: Currently it is not possible to upload different assets for different locales! (Planned feature)**

In order to enable asset management for non programmers, `matestack-cms` offers
asset upload/destroy functionality within the prebuilt Admin UI pages

**Limitation**

Currently `matestack-cms` does not support `Arrays` in the `yml` files:

`db/cms/seeds/content_items/en.yml`

```yml
en:
  hello: "Hello world"
  # below not supported
  foos:
    - "baz!"
    - "foo!"
  # use this instead
  foos:
    1: "baz!"
    2: "foo!"
```

## Installation

### Dependencies

`matestack-cms` requires:

- Mobility for translations
- ActiveRecord for persistence
- ActiveStorage for asset uploads
- Redis for caching (optional, but highly recommended)
- matestack-ui-material for prebuilt admin UIs (optional)

### Access Token

You need a valid access token in order to install matestack-cms/matestack-ui-material.
You can get one from jonas@matestack.io

Make the access token accessible via a Bundler ENV var:

```bash
$ export BUNDLE_GITLAB__COM=x-access-token:<token>
```

If you're using Docker/docker-compose, pass in the ENV var form the host system to the container using:

```yml
version: "3"
services:
  rails:
    #...
    environment:
      BUNDLE_GITLAB__COM: $BUNDLE_GITLAB__COM
    #...
```

### Gem Setup

Add these lines to your applications Gemfile:

```ruby
gem 'mobility', '~> 0.8.9'
gem 'matestack-cms', git: "https://gitlab.com/matestack/matestack-cms.git"
```

And then execute:

```bash
$ bundle install
```

### Setup Mobility

In order to setup `mobility` run its install rake task which gives you a migration and an initializer:

```bash
$ rails generate mobility:install
$ bundle exec rake db:migrate
```

The initializer should contain following configuration:

`app/config/initializers/mobility.rb`

```ruby
Mobility.configure do |config|
  config.default_backend = :key_value
  config.accessor_method = :translates
  config.query_method    = :i18n
end
```

### Setup ActiveStorage

If not already happened, configure your application to use ActiveStorage https://edgeguides.rubyonrails.org/active_storage_overview.html

```bash
$ bundle exec rake active_storage:install
```

and then run them as usual with:

```bash
$ bundle exec rake db:migrate
```

Add a configuration file in order to tell `ActiveStorage` how to store uploads:

(Example for using the local disk service)

`app/config/storage.yml`

```yml
test:
  service: Disk
  root: <%= Rails.root.join("tmp/storage") %>

local:
  service: Disk
  root: <%= Rails.root.join("storage") %>
```

_Note:_ If you're using Docker in order to run/deploy your application and are
using the local disk service as your storage, you have to mount the storage folder to
the host system:

`docker-compose.example.yml`

```yml
version: "3"
services:
  rails:
    #...
    volumes:
      - storage-volume:/app/storage # this path might be configured differently in your setup
    #...

volumes:
  storage-volume:
```

### Setup Redis

`matestack-cms` uses `Redis` for caching in order to increase rendering performance.

Currently the connection is not configurable. The `Redis` server has to be available with a
host name "redis" (not localhost or something else). This is easily done with:

```yml
version: "3"
services:
  rails:
    #...
    links:
      - redis
    #...

  redis:
    image: redis:5.0.7-alpine
    volumes:
      - redis-volume:/data

  # optional for debugging (local only!)
  redis_gui:
    image: marian/rebrow
    links:
      - redis
    ports:
      - "5001:5001"

volumes:
  redis-volume:
  #...
```

### Setup Matestack::Cms

**Available Locales**

Add an initializer in order to define available locales:

`config/initializers/locales.rb`

```ruby
I18n.available_locales = [:de, :en] # for example
```

Remember to restart your Rails server after adding/changing this file!

**Migrations**

`matestack-cms` uses a table in your applications database in order to store the content.

You can copy the migration into your applications `db/migrate` folder with:

```bash
$ bundle exec rake matestack_cms:install:migrations
```

and then run them as usual with:

```bash
$ bundle exec rake db:migrate
```

**Helpers**

Include the helpers to your application:

`app/controllers/application_controller.rb`

```ruby
class ApplicationController < ActionController::Base

  include Matestack::Cms::ApplicationHelper

end
```

**Routes**

`matestack-cms` comes with controller endpoints allowing to edit content from an UI.
Typically, these endpoints should be protected. **User authentication has to be handled by your app.**

Following routes are made for an application which uses the `devise` gem for authentication and has a `devise` model
named `Admin`. Furthermore this `Admin` model may have a method allowing to check if the admin belongs to a certain role.

_API routes for create/update/delete content and upload/delete assets_

`app/config/routes.rb`

```ruby
# ...
# routes for create/update/delete
# only check for authorized admins
authenticate :admin do
  mount Matestack::Cms::Engine => "/cms"
end
# OR:
# check if the current_admin has a specific role
authenticate :admin, lambda {|admin| admin.has_role_xyz? } do
  mount Matestack::Cms::Engine => "/cms", defaults: { locale: nil } #please set all of your defaults to nil
end
```

(TODO: Detailed API documentation)

This gives you following routes:

- `upload_asset_content_item_path`
  - POST /content_items/:id/upload_asset(.:format) --> matestack/cms/content_items#upload_asset

* `delete_asset_content_item_path`
  - DELETE /content_items/:id/asset/:key(.:format) --> matestack/cms/content_items#delete_asset

- `content_items_path`
  - POST /content_items(.:format) --> matestack/cms/content_items#create

* `edit_content_item_path`
  - GET /content_items/:id/edit(.:format) --> matestack/cms/content_items#edit

- `content_item_path`
  - PATCH /content_items/:id(.:format) --> matestack/cms/content_items#update
  - PUT /content_items/:id(.:format) --> matestack/cms/content_items#update
  - DELETE /content_items/:id(.:format) --> matestack/cms/content_items#destroy

You may find the controller implementation here:

`matestack-cms-engine-repo/app/controllers/matestack/cms/content_items_controller.rb`

_View routes for create/update/delete content and upload/delete assets_

(TODO: get rid of this route helper dependency)

These controller actions are currently designed to tightly interact with an Admin UI.
That's why you have to define the routes rendering the views for editing the content like this:

`app/config/routes.rb`

```ruby
# ...
# basic usage
resources :content_items, only: [:index, :edit, :new]
# if you want to use a namespaced controller add the module like:
resources :content_items, only: [:index, :edit, :new], module: 'administration'
# and if you want to define a specific path for these routes, add the path option like:
resources :content_items, only: [:index, :edit, :new], module: 'administration', path: 'administration/content_items/' do
  collection do
    get "search_results", to: 'content_items#search_results', as: "search_results"
  end
end
```

All of the above shown route definitions generate `content_items_path`, `edit_content_item_path`, `new_content_item_path` helper. These path helper methods are used within the engines controller actions in order to redirect properly.

**SVG Files**

If you want to use SVG images through `matestack-cms`, you need to add this configuration to

`config/application.rb`

```ruby
#...
config.active_storage.content_types_to_serve_as_binary -= ['image/svg+xml']
#...
```

**Admin UI**

`matestack-cms` comes with a prebuilt Admin UI. Currently it is only available alongside `matestack-ui-material`. Therefore add the gem like:

(TODO: Add documentation/prebuilt views for non matestack Admin UIs)

```ruby
gem 'matestack-ui-core'
gem 'matestack-ui-material', git: "https://gitlab.com/matestack/matestack-ui-material.git"
```

And then execute:

```bash
$ bundle install
```

Based on routes like

```ruby
resources :content_items, only: [:index, :edit, :new], module: 'administration', path: 'administration/content_items/'
```

and an already implemented `administration` matestack-ui-material app with `devise` authentication in place,

add a controller
`app/controllers/administration/content_items_controller.rb`

```ruby
class Administration::ContentItemsController < ApplicationController

  before_action :authenticate_admin!

  def index
    responder_for(Pages::Administration::ContentItems::Index)
  end

  def edit
    responder_for(Pages::Administration::ContentItems::Edit)
  end

  def new
    responder_for(Pages::Administration::ContentItems::New)
  end

  def search_results
    responder_for(Pages::Administration::ContentItems::SearchResults)
  end

end
```

and the matestack pages, which inherit from the prebuilt engine pages:

`app/matestack/pages/administration/content_items/index.rb`

```ruby
class Pages::Administration::ContentItems::Index < Matestack::Cms::Pages::ContentItems::Index

end
```

`app/matestack/pages/administration/content_items/edit.rb`

```ruby
class Pages::Administration::ContentItems::Edit < Matestack::Cms::Pages::ContentItems::Edit

end
```

`app/matestack/pages/administration/content_items/new.rb`

```ruby
class Pages::Administration::ContentItems::New < Matestack::Cms::Pages::ContentItems::New

end
```

`app/matestack/pages/administration/content_items/search_results.rb`
```ruby
class Pages::Administration::ContentItems::SearchResults < Matestack::Cms::Pages::ContentItems::SearchResults

end
```

Add the stylesheets to your administration styles:

`app/assets/stylesheets/administration.css`

```css
/* ...
*= require matestack-ui-material
*= require matestack-cms
*/
```

Add the javascripts to your administration javascipt:

`app/assets/javascripts/administration.js`

```js
//= require matestack-ui-core
//= require matestack-ui-material
//= require matestack-cms
```

### Setup Language Switch

To setup a language switch you have to follow these steps:

1. Update routes
2. Define default and available locales
3. Use locale in the application
4. Implement language switch

#### Update routes

Add a scope around the routes you want to make available in different languages. Constraint your scope to the languages you want to provide. In this example we constraint locale to be one off 'de' or 'en' if not `nil`.
Make sure you define your `root` Route outside of this scope.

`config/routes.rb`

```ruby
root to: 'home#index'
scope "(:locale)", locale: /de|en/ do
  get '/', to: 'home#index'
  get '/about', to: 'about#index'
end
```

#### Define default and available locales

In your `application.rb` file set your default locale.

`config/application.rb`

```ruby
  config.i18n.default_locale = :de
```

Add an initializer or update your I18n intializer to set your available_locales

`config/initializers/i18n.rb`

```ruby
  I18n.available_locales = [:de, :en]
```

#### Use locale in the application

Now you have to use the `locale` param in your routes to stay with the same language when switching between routes.
Passing the `locale` could be done by setting it in your path helpers.

```ruby
home_path(locale: I18n.locale)
```

or passing it as default url option to your routes.

`application.rb`

```ruby
Rails.application.routes.default_url_options[:locale] = I18n.locale
```

To set your locale and default url options depending on the scope parameter `locale`, add a before action in your application controller or wherever you need it.

`application_controller.rb`

```ruby
class ApplicationController < ActionController::Base
  before_action :set_locale

  def set_locale
    # set locale to given locale or use default
    I18n.locale = params[:locale] ||= I18n.default_locale
    Rails.application.routes.default_url_options[:locale] = I18n.locale
  end

end
```

If you want to only scope routes for not default languages change your `set_locale` method accordingly:

```ruby
def set_locale
  I18n.locale = params[:locale] ||= I18n.default_locale
  Rails.application.routes.default_url_options[:locale] = params[:locale]
  # or
  locale = I18n.locale == I18n.default_locale ? nil : I18n.locale
  Rails.application.routes.default_url_options[:locale] = locale
end
```

#### Implement language switch

The language switch is now as simple as a link with the correct locale as parameter.
Like `root_path(locale: :en)` to switch to english or `root_path(locale: :de) to switch to german.

If you want the user to stay on the same page they are currently on use `url_for({ locale: :en })`. To be able to use the `url_for` helper in an app, page or component you have to include `ActionView::RoutingUrlFor` where you want to use it.

## Usage

TODO

### Seed/migrate content

### Render content

### Edit content

### Export content

## Contribute

### Setup the dummy app

Remember to setup the gitlab token on your system as describe above.

**Build**

```bash
$ docker-compose build --build-arg BUNDLE_GITLAB__COM dummy
```

**Prepare the database**

```bash
$ docker-compose run dummy bash
$ cd spec/dummy
$ bundle exec rake db:migrate
$ bundle exec rake db:seed
$ bundle exec matestack_cms:seed
```

**Run the app**

```bash
$ docker-compose up dummy
```

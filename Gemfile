source 'https://rubygems.org'
git_source(:github) { |repo| 'https://github.com/#{repo}.git' }

# Declare your gem's dependencies in matestack-cms.gemspec.
# Bundler will treat runtime dependencies like base dependencies, and
# development dependencies will be added by default to the :development group.
gemspec

# Declare any dependencies that are still in development here instead of in
# your gemspec. These might include edge Rails or gems from your path or
# Git. Remember to move these dependencies to your gemspec before releasing
# your gem to rubygems.org.

# To use a debugger
# gem 'byebug', group: [:development, :test]

# The mobility gem has to be added here in order to be properly required within the dummy app
gem 'mobility', '~> 0.8.9'

# matestack-ui-material is required to render the prebuilt admin pages within the dummy app
# you need to add your token as bundler ENV var like: export BUNDLE_GITLAB__COM=x-access-token:<token>
gem 'matestack-ui-material', git: 'https://gitlab.com/matestack/matestack-ui-material.git'

group :development, :test do
  gem 'rspec'
  gem 'rspec-rails', '~> 3.8'
  gem 'capybara'
  gem 'selenium-webdriver', '~> 3.142', '>= 3.142.7'
  gem 'puma'
  gem 'simplecov', require: false, group: :test
  gem 'byebug'
  gem 'webmock'
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'devise'
  gem 'pg'
  gem 'matestack-ui-core'
end

group :test do
  gem 'generator_spec'
  gem 'rspec-wait', '~> 0.0.9'
end
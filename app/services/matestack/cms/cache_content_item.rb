module Matestack
  module Cms

    class CacheContentItem
    
      attr_accessor :content_item

      def self.call(*args, &block)
        new(*args, &block).save_in_cache
      end
    
      def initialize(content_item)
        @content_item = content_item
      end
    
      def save_in_cache
        ContentItem.available_locales.each do |locale|
          save_in_cache_for_locale(locale)
        end
        update_parent_caches if content_item.parents.any? 
      end

      private
      
      def save_in_cache_for_locale(locale, item = content_item)
        Mobility.locale = locale
    
        value_to_be_cached = nil
        if item.content_items.any?
          value_to_be_cached = item.processed_value(locale).to_json
        elsif item.value.present?
          value_to_be_cached = item.value
        elsif item.asset.attached?
          value_to_be_cached = Rails.application.routes.url_helpers.rails_blob_path(item.asset, only_path: true)
        end
    
        unless value_to_be_cached.nil?
          full_key_path = [locale].concat(
            item.parents.pluck(:key).push(item.key)
          ).join('.')
          ContentItem.redis.set(full_key_path, value_to_be_cached)
        end
      end
    
      def update_parent_caches
        content_item.parents.each_with_index do |parent, index|
          ContentItem.available_locales.each do |locale|
            full_key_path = [locale].concat(
              content_item.parents[0..index].pluck(:key)
            ).join('.')
            if ContentItem.redis.get(full_key_path).present?
              save_in_cache_for_locale(locale, ContentItem.find(parent[:id]))
            end
          end
        end
      end
    
    end

  end
end

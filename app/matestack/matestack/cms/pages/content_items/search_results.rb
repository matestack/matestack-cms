module Matestack
  module Cms
    class Pages::ContentItems::SearchResults < Matestack::Ui::Page

      include Matestack::Cms::Pages::ContentItems::SharedPartials

      def prepare
        @result_items = {}
        query_param = context[:params][:value]
        unless query_param.blank?
          I18n.available_locales.each do |locale|
            Mobility.locale = locale
            @result_items[locale] = Matestack::Cms::ContentItem.i18n do
              value.matches("%#{query_param}%")
            end
          end
          I18n.available_locales.each do |locale|
            if @results.nil?
              @results = @result_items[locale]
            else
              @results.merge(@result_items[locale])
            end
          end
          @results = @results.distinct
        else
          @results = []
        end
      end

      def response
        components {
          partial :title_section
          material_grid do
            @results.each do |content_item|
              material_grid_cell desktop: 3 do
                partial :item_card, content_item: content_item, show_parent_keys: true
              end
            end
          end
          partial :search_section
        }
      end

      protected

      def title_section
        partial {
          material_grid do
            material_grid_cell desktop: 12 do
              material_heading size: 3, text: "Content Element Suche"
              strong class: "mdc-typography--body2", text: "nach: #{context[:params][:value]}"
            end
          end
        }
      end

      def search_section
        partial {
          material_grid do
            material_grid_cell do
              form search_config, :include do
                material_form_input key: :value, type: :text, material_type: :outlined, label: "Value"
                paragraph
                form_submit do
                  material_button type: :raised, text: "Suchen"
                end
              end
            end
          end
        }
      end

      def search_config
        {
          for: :search,
          method: :post,
          path: Matestack::Cms::Engine.routes.url_helpers.search_content_items_path,
          success: {
            transition: {
              follow_response: true
            }
          }
        }
      end

    end
  end
end

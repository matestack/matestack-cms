module Matestack
  module Cms
    class Pages::ContentItems::Index < Matestack::Ui::Page

      include Matestack::Cms::Pages::ContentItems::SharedPartials

      def prepare
        @root_items = Matestack::Cms::ContentItem.where(matestack_cms_content_item_id: nil)
        @asset_items = Matestack::Cms::ContentItem.joins(:asset_attachment)
      end

      def response
        components {
          partial :title_section
          partial :list_section, content_items: @root_items, show_assets: false
          partial :assets_section
        }
      end

      protected

      def title_section
        partial {
          material_grid do
            material_grid_cell desktop: 12 do
              material_heading size: 3, text: "Content Elemente"
              strong class: "mdc-typography--body1", text: "Suchen"
              paragraph
              form search_config, :include do
                material_form_input key: :value, type: :text, material_type: :outlined, label: "Value"
                paragraph
                form_submit do
                  material_button type: :raised, text: "Suchen"
                end
              end
            end
          end
        }
      end

      def add_section
        partial {
          material_grid do
            material_grid_cell do
              transition path: :new_content_item_path do
                material_button type: :outlined, text: "Content anlegen", icon: "add"
              end
            end
          end
        }
      end


      def assets_section
        partial {
          material_grid  do
            material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
              material_heading size: 4, text: "Übersicht aller Medien"
            end
            @asset_items.each do |item|
              next if item.asset.blob.content_type == "application/pdf"
              partial :asset_card, content_item: item
            end
          end
          material_grid  do
            material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
              material_heading size: 4, text: "Übersicht aller Dokumente"
            end
            @asset_items.each do |item|
              next unless item.asset.blob.content_type == "application/pdf"
              partial :asset_card, content_item: item
            end
          end
        }
      end

      def search_config
        {
          for: :search,
          method: :post,
          path: Matestack::Cms::Engine.routes.url_helpers.search_content_items_path,
          success: {
            transition: {
              follow_response: true
            }
          }
        }
      end

    end
  end
end

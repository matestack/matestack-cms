module Matestack
  module Cms
    class Pages::ContentItems::Edit < Matestack::Ui::Page

      include Matestack::Cms::Pages::ContentItems::SharedPartials

      def prepare
        @content_item = Matestack::Cms::ContentItem.find context[:params][:id]
        @parents = @content_item.parents
        @parent = @content_item.parent
        @is_collection_container = @content_item.is_collection_container?
        if @parent.present?
          @parent_is_collection_container = @content_item.parent.is_collection_container?
        else
          @parent_is_collection_container = false
        end
      end

      def response
        components {
          partial :title_section
          if @parent_is_collection_container
            material_grid do
              material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
                material_heading size: 4, text: "Element '#{@content_item.key}' aus Sammlung '#{@content_item.parent.key}'"
                if @content_item.active?
                  action deactivate_action_config do
                    material_button type: :outlined, text: "Element deaktivieren"
                  end
                else
                  action activate_action_config do
                    material_button type: :outlined, text: "Element aktivieren"
                  end
                end
                paragraph
                if @parent.content_items.count > 1
                  action delete_action_config do
                    material_button type: :outlined, text: "Element löschen"
                  end
                else
                  material_button type: :outlined, text: "Element löschen", disabled: true
                  small class: "mdc-typography--body2" do
                    plain "Element kann nicht gelöscht werden, da eine Sammlung mindestens aus einem Element bestehen muss"
                  end
                end
                paragraph
                async hide_on: "show_settings" do
                  onclick emit: "show_settings" do
                    material_button type: :outlined, text: "Sortierung ändern"
                  end
                end
              end
            end
            async show_on: "show_settings" do
              partial :settings_section
            end
          end
          if @content_item.content_items.empty? && !@content_item.asset.attached?
            partial :form_section
          end
          if @content_item.content_items.empty? && @content_item.all_values_nil?
            partial :asset_section
          end
          if @content_item.all_values_nil? && !@content_item.asset.attached?
            # material_grid do
            #   material_grid_cell do
            #     material_heading size: 5, text: "Kinder Elemente anlegen"
            #   end
            # end
            # material_grid do
            #   material_grid_cell do
            #     transition path: :new_content_item_path, params: { parent_id: @content_item.id } do
            #       material_button type: :outlined, text: "Content anlegen", icon: "add"
            #     end
            #   end
            # end
          end
          if @content_item.content_items.any?
            partial :list_section, content_items: @content_item.content_items.sort_by { |k,v| k.to_s.to_i }, can_duplicate: @content_item.is_collection_container?
          end
        }
      end

      protected

      def title_section
        partial {
          material_grid do
            material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
              material_heading size: 3, text: "Content bearbeiten"
              material_heading size: 5 do
                transition path: :content_items_path do
                  plain "root"
                end
                icon class: "material-icons", text: "chevron_right", attributes: { style: "top: 5px; position: relative;" }
                if @parents.any?
                  @parents.each do |parent|
                    transition path: :edit_content_item_path, params: { id: parent[:id] } do
                      plain parent[:key]
                    end
                    icon class: "material-icons", text: "chevron_right", attributes: { style: "top: 5px; position: relative;" }
                  end
                end
                strong text: @content_item.key
              end
            end
          end
        }
      end

      def form_section
        partial {
          Matestack::Cms::ContentItem.available_locales.each do |locale|
            material_grid  do
              material_grid_cell desktop: 12 do
                material_heading size: 5, text: "Text eingeben für Sprache: #{locale}"
              end
            end
            material_grid  do
              material_grid_cell desktop: 12 do
                form edit_form_config(locale), :include do
                  material_form_input material_type: :outlined, key: "value_#{locale}", type: :textarea
                  paragraph
                  form_submit do
                    material_button type: :raised, text: "speichern"
                  end
                end
              end
            end
          end
        }
      end

      def settings_section
        partial {
          material_grid  do
            material_grid_cell desktop: 12 do
              form edit_form_config, :include do
                material_form_input material_type: :outlined, key: :key, type: :text
                paragraph
                form_submit do
                  material_button type: :raised, text: "Key ändern"
                end
              end
            end
          end
        }
      end

      def asset_section
        partial {
          material_grid  do
            material_grid_cell desktop: 12 do
              material_heading size: 5, text: "Datei hochladen"
            end
          end
          partial :asset_upload
          # matestack_cms_contentItems_asset content_item: @content_item
        }
      end

      def edit_form_config(locale=nil)
        {
          for: @content_item,
          method: :put,
          path: Matestack::Cms::Engine.routes.url_helpers.content_item_path(id: @content_item.id, locale_to_be_updated: locale),
          success: {
            emit: "form_success",
            transition: {
              follow_response: true
            }
          },
          failure: {
            emit: "form_failure"
          }
        }
      end

      def delete_action_config
        {
          method: :delete,
          path: Matestack::Cms::Engine.routes.url_helpers.content_item_path(id: @content_item.id),
          confirm: {
            text: "Wenn Sie dieses Element löschen, werden auch alle Kinder Elemente gelöscht!"
          },
          success: {
            emit: "action_success",
            transition: {
              follow_response: true
            }
          },
          failure: {
            emit: "action_failure"
          }
        }
      end

      def deactivate_action_config
        {
          method: :put,
          path: Matestack::Cms::Engine.routes.url_helpers.deactivate_content_item_path(id: @content_item.id),
          success: {
            emit: "action_success",
            transition: {
              follow_response: true
            }
          },
          failure: {
            emit: "action_failure"
          }
        }
      end

      def activate_action_config
        {
          method: :put,
          path: Matestack::Cms::Engine.routes.url_helpers.activate_content_item_path(id: @content_item.id),
          success: {
            emit: "action_success",
            transition: {
              follow_response: true
            }
          },
          failure: {
            emit: "action_failure"
          }
        }
      end

      def asset_upload
        partial {
          material_grid do
            material_grid_cell desktop: 3 do
              begin
                delete_attachment_config = {
                  method: :delete,
                  path: Matestack::Cms::Engine.routes.url_helpers.delete_asset_content_item_path(id: @content_item.id, key: :asset),
                  success: {
                    emit: "action_success",
                    transition: {
                      follow_response: true
                    }
                  },
                  failure: {
                    emit: "action_failure"
                  }
                }
                media_slot = slot {
                  if @content_item.asset.blob.content_type == "application/pdf"
                    icon class: "material-icons", text: "description"
                  else
                    img path: ::Rails.application.routes.url_helpers.rails_blob_path(@content_item.asset, only_path: true)
                  end
                }
                actions_slot = slot {
                  cms_asset_upload key: :asset, upload_path: Matestack::Cms::Engine.routes.url_helpers.upload_asset_content_item_path(id: @content_item.id), text: "ändern"
                  action delete_attachment_config do
                    material_button text: "löschen"
                  end
                }
              rescue
                media_slot = nil
                actions_slot = slot {
                  cms_asset_upload key: :asset, upload_path: Matestack::Cms::Engine.routes.url_helpers.upload_asset_content_item_path(id: @content_item.id), text: "hochladen"
                }
              end
              material_card headline: "Datei", media_slot: media_slot, actions_slot: actions_slot
            end
          end
        }
      end

    end
  end
end

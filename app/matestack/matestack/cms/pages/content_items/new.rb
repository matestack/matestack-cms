module Matestack
  module Cms
    class Pages::ContentItems::New < Matestack::Ui::Page

      def prepare
        @content_item = Matestack::Cms::ContentItem.new
        if context[:params][:parent_id].present?
          @content_item.matestack_cms_content_item_id = context[:params][:parent_id]
        end
        @parents = @content_item.parents
      end

      def response
        components {
          partial :title_section
          partial :form_section
        }
      end

      protected

      def title_section
        partial {
          material_grid do
            material_grid_cell desktop: 12 do
              material_heading size: 3, text: "Content anlegen"
              material_heading size: 5 do
                transition path: :content_items_path do
                  plain "root"
                end
                icon class: "material-icons", text: "chevron_right", attributes: { style: "top: 5px; position: relative;" }
                if @parents.any?
                  @parents.each do |parent|
                    transition path: :edit_content_item_path, params: { id: parent[:id] } do
                      plain parent[:key]
                    end
                    icon class: "material-icons", text: "chevron_right", attributes: { style: "top: 5px; position: relative;" }
                  end
                end
              end
            end
          end
        }
      end

      def form_section
        partial {
          material_grid  do
            material_grid_cell desktop: 12 do
              form new_form_config, :include do
                material_form_input material_type: :outlined, key: :key, type: :text, label: "Key"
                paragraph
                form_submit do
                  material_button type: :raised, text: "anlegen"
                end
              end
            end
          end
        }
      end

      def new_form_config
        {
          for: @content_item,
          method: :post,
          path: Matestack::Cms::Engine.routes.url_helpers.content_items_path(matestack_cms_content_item_id: context[:params][:parent_id]),
          success: {
            emit: "form_success",
            transition: {
              follow_response: true
            }
          },
          failure: {
            emit: "form_failure"
          }
        }
      end

    end
  end
end

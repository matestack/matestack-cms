module Matestack
  module Cms
    module Pages::ContentItems::SharedPartials

      def list_section content_items: [], show_assets: true, can_duplicate: false, show_parent_keys: false
        partial {
          material_grid  do
            material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
              material_heading size: 4, text: "Container"
            end
            i = 0
            content_items.each do |item|
              next unless item.content_items.any?
              partial :container_card, content_item: item, can_duplicate: can_duplicate, show_parent_keys: show_parent_keys
              i += 1
            end
            if i == 0
              material_grid_cell desktop: 12, class: "content-items__list--no-elements" do
                small class: "mdc-typography--body2", text: "keine Container vorhanden"
              end
            end
          end
          material_grid  do
            material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
              material_heading size: 4, text: "Texte"
            end
            i = 0
            content_items.each do |item|
              next if item.content_items.any? || item.asset.attached?
              partial :item_card, content_item: item, can_duplicate: can_duplicate, show_parent_keys: show_parent_keys
              i += 1
            end
            if i == 0
              material_grid_cell desktop: 12, class: "content-items__list--no-elements" do
                small class: "mdc-typography--body2", text: "keine Texte vorhanden"
              end
            end
          end
          if show_assets == true
            material_grid  do
              material_grid_cell desktop: 12, class: "mdc-layout-grid__cell--condensed" do
                material_heading size: 4, text: "Medien"
              end
              i = 0
              content_items.each do |item|
                next unless item.asset.attached?
                partial :asset_card, content_item: item
                i += 1
              end
              if i == 0
                material_grid_cell desktop: 12, class: "content-items__list--no-elements" do
                  small class: "mdc-typography--body2", text: "keine Medien vorhanden"
                end
              end
            end
          end
        }
      end

      def asset_card content_item: nil, can_duplicate: false, show_parent_keys: false
        partial {
          material_grid_cell desktop: 3 do
            media_slot = slot {
              if content_item.asset.blob.content_type == "application/pdf"
                icon class: "material-icons", text: "description"
              else
                img path: ::Rails.application.routes.url_helpers.rails_blob_path(content_item.asset, only_path: true)
              end
            }
            actions_slot = slot {
              transition path: :edit_content_item_path, params: { id: content_item.id } do
                material_button text: "bearbeiten"
              end
              link path: ::Rails.application.routes.url_helpers.rails_blob_path(content_item.asset, only_path: true), target: "_blank" do
                material_button text: "vorschau"
              end
            }
            headline_slot = slot {
              plain content_item.key
              br
              br
              small class: "content" do
                plain content_item.asset.blob.filename
              end
            }
            material_card headline_slot: headline_slot, media_slot: media_slot, actions_slot: actions_slot
          end
        }
      end

      def container_card content_item: nil, can_duplicate: false, show_parent_keys: false
        partial {
          material_grid_cell desktop: 3 do
            media_slot = slot {
              icon class: "material-icons", text: "file_copy"
            }
            actions_slot = slot {
              transition path: :edit_content_item_path, params: { id: content_item.id } do
                material_button text: "öffnen"
              end
              if can_duplicate
                action duplicate_config(content_item.id) do
                  material_button text: "duplizieren"
                end
              end
            }
            headline_slot = slot {
              plain "Element" if content_item.parent.present? && content_item.parent.is_collection_container?
              plain content_item.key
              plain "(not active)" if !content_item.active?
              br
              br
              small class: "content" do
                plain "Enthält"
                plain content_item.content_items.count
                plain "weitere  Elemente:"
                br
                strong text: content_item.content_items.pluck(:key).take(4).join(", ").truncate(30) unless content_item.content_items.empty?
              end
            }
            material_card headline_slot: headline_slot , media_slot: media_slot, actions_slot: actions_slot
          end
        }
      end

      def item_card content_item: nil, can_duplicate: false, show_parent_keys: false
        partial {
          material_grid_cell desktop: 3 do
            media_slot = slot {
              icon class: "material-icons", text: "subject"
            }
            actions_slot = slot {
              transition path: :edit_content_item_path, params: { id: content_item.id } do
                material_button text: "bearbeiten"
              end
              if can_duplicate
                action duplicate_config(content_item.id) do
                  material_button text: "duplizieren"
                end
              end
            }
            headline_slot = slot {
              if show_parent_keys
                small class: "content" do
                  content_item.parents.each do |parent|
                    plain parent[:key]
                    plain ">"
                  end
                end
                br
                br
              end
              plain "Element" if content_item.parent.present? && content_item.parent.is_collection_container?
              plain content_item.key
              plain "(not active)" if !content_item.active?
              br
              br
              I18n.available_locales.each do |locale|
                small class: "content" do
                  strong text: "#{locale.upcase}: "
                  plain content_item.send("value_#{locale}").truncate(30) unless content_item.send("value_#{locale}").nil?
                end
                br
              end
            }
            material_card headline_slot: headline_slot , media_slot: media_slot, actions_slot: actions_slot
          end
        }
      end

      def duplicate_config content_item_id
        {
          method: :post,
          path: Matestack::Cms::Engine.routes.url_helpers.deep_duplicate_content_item_path(id: content_item_id),
          success: {
            emit: "action_success",
            transition: {
              follow_response: true
            }
          },
          failure: {
            emit: "action_failure"
          }

        }
      end


    end
  end
end

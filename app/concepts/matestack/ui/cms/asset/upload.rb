# JS and CSS defined in engine/vendor/assets (no builder app present currently --> quick fix)

module Matestack::Ui::Cms::Asset
  class Upload < Matestack::Ui::DynamicComponent

    def setup
      @component_config[:key] = @options[:key]
      @component_config[:upload_path] = @options[:upload_path]
    end

    def response
      components {
        div id: "matestack-ui-cms-asset-upload" do
          material_button do
            icon class: "material-icons mdc-button__icon", text: @options[:icon] if @options[:icon]
            span class: "mdc-button__label", text: @options[:text] ||= "upload"
            input class:"mdc-text-field__input hidden-input", attributes: {"placeholder":"File", "type": "text", "v-bind:value": "this.file.name", "readonly": true}
            div class: "file-input" do
              input type: "file", id:"file", attributes: {"ref":"file", "v-on:change":"handleFileUpload()"}
            end
          end
        end
      }
    end

  end
end

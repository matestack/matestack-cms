require "redis"

module Matestack
  module Cms
    class ContentItem < ApplicationRecord
      include Enumerable
      extend Mobility

      translates :value, type: :text, fallthrough_accessors: true

      has_one_attached :asset
      belongs_to :parent, optional: true, class_name: "Matestack::Cms::ContentItem", foreign_key: "matestack_cms_content_item_id"
      has_many :content_items, dependent: :destroy, class_name: "Matestack::Cms::ContentItem", foreign_key: "matestack_cms_content_item_id"
      validates :key, presence: true

      default_scope { order(id: :asc) }

      @available_locales = I18n.available_locales

      @redis = Redis.new(host: "redis")

      class << self
        attr_accessor :available_locales
        attr_accessor :redis
      end

      attr_accessor :skip_cache_content_item
      after_save :cache_content_item, unless: :skip_cache_content_item

      def cache_content_item
        CacheContentItem.(self)
      end

      def self.build_cache
        self.all.each do |content_item|
          content_item.save_in_cache
        end
      end

      def self.seed
        self.parse_and_write_to_db(true)
      end

      def self.extend
        self.parse_and_write_to_db(false)
      end

      def self.parse_and_write_to_db(overwrite_if_exists)
        files = Dir["#{Rails.root.to_s}/db/cms/seeds/content_items/**/*.{yml}"]
        files.each do |file_path|
          seed_as_hash = YAML.load_file(file_path)
          locale = seed_as_hash.keys.first
          content_as_hash = seed_as_hash[locale]
          self.create_from_hash(locale, content_as_hash, nil, overwrite_if_exists)
        end
      end

      def self.create_from_hash(locale, content_as_hash, parent_id, overwrite_if_exists)
        content_as_hash.each do |key, value|
          if value.is_a?(Hash)
            content_item = self.find_or_create_by(key: key, matestack_cms_content_item_id: parent_id)
            if !content_item.new_record? && self.is_collection_hash?(value) && !overwrite_if_exists && content_item.content_items.any?
              # do not recreate collection items from seeds if collection parent already exists with existing collection items and overwrite is false
            else
              self.create_from_hash(locale, value, content_item.id, overwrite_if_exists)
            end
          else
            if value.is_a?(String) && value.starts_with?("asset:")
              content_item = self.find_or_initialize_by(key: key, matestack_cms_content_item_id: parent_id)
              if content_item.new_record?
                content_item.save!
              end
              if content_item.asset.attached?
                if overwrite_if_exists
                  content_item.asset.attach(io: File.open("#{Rails.root.to_s}/db/cms/seeds/assets/#{value.gsub("asset:", "")}"), filename: value.gsub("asset:", ""))
                end
              else
                content_item.asset.attach(io: File.open("#{Rails.root.to_s}/db/cms/seeds/assets/#{value.gsub("asset:", "")}"), filename: value.gsub("asset:", ""))
              end
            else
              content_item = self.find_or_initialize_by(key: key, matestack_cms_content_item_id: parent_id)
              if content_item.new_record?
                content_item.save!
              end
              if content_item.value_backend.read(locale.to_sym).present?
                if overwrite_if_exists
                  content_item.send(:value=, value, locale: locale.to_sym)
                end
              else
                content_item.send(:value=, value, locale: locale.to_sym)
                content_item.save!
              end
            end
          end
        end
      end

      def self.export_as_seeds(locale)
        Mobility.locale = locale
        result = {}
        result[locale] = {}
        root_content_items = ContentItem.where(matestack_cms_content_item_id: nil)
        root_content_items.each do |content_item|
          result[locale][content_item.key] = content_item.to_hash
        end

        return result
      end

      def self.resolve_from_cache query_string, locale
        full_key_path = [locale, query_string].join(".")
        content_item = ContentItem.redis.get(full_key_path)
        value = JSON.parse(content_item).deep_symbolize_keys rescue content_item
      end

      def self.resolve_content query_string, locale
        Mobility.locale = locale
        from_cache = self.resolve_from_cache(query_string, locale)

        unless from_cache.nil?
          return from_cache
        else
          content_item = resolve_content_item(query_string)

          if content_item.nil?
            raise "ContentItem not found"
          else
            value = content_item.processed_value(locale)
            if value.nil?
              return "#{query_string} not set"
            else
              if value.is_a?(Hash)
                return value
              else
                content_item.save_in_cache
                return value
              end
            end
          end

        end
      end

      def self.resolve_content_item(query_string)
        if query_string.include?(".")
          query_string_parts = query_string.split(".")
          root_node = self.find_by(key: query_string_parts[0], matestack_cms_content_item_id: nil)
          node = root_node
          query_string_parts[1..-1].each do |node_key|
            node = node.content_items.find_by(key: node_key)
          end
        else
          node = self.find_by(key: query_string, matestack_cms_content_item_id: nil)
        end

        return node
      end

      def self.is_collection_hash?(hash)
        hash.keys.all? { |key| Integer(key.to_s) rescue nil }
      end

      def save_in_cache
        ContentItem.available_locales.each do |locale|
          self.save_in_cache_for_locale(locale)
        end
      end

      def save_in_cache_for_locale(locale)
        Mobility.locale = locale

        value_to_be_cached = nil

        if self.content_items.any?
          value_to_be_cached = self.to_hash(locale)
        elsif self.value.present?
          value_to_be_cached = self.value
        elsif self.asset.attached?
          value_to_be_cached = Rails.application.routes.url_helpers.rails_blob_path(self.asset, only_path: true)
        end

        full_key_path = []
        full_key_path << locale

        unless value_to_be_cached.nil?
          self.parents.each do |parent|
            full_key_path << parent[:key]
          end
          full_key_path << self.key

          ContentItem.redis.set(full_key_path.join("."), value_to_be_cached)
        end
      end

      def processed_value(locale)
        Mobility.locale = locale

        if self.content_items.any?
          return self.to_hash(locale)
        elsif self.value.present?
          return self.value
        elsif self.asset.attached?
          return Rails.application.routes.url_helpers.rails_blob_path(self.asset, only_path: true)
        end
      end

      def to_hash(locale)
        result = {}
        if self.content_items.any?
          self.content_items.each do |child_item|
            result[child_item.key.to_sym] = build_hash_node(child_item, locale) if child_item.active?
          end
          return result
        else
          return result
        end
      end

      def build_hash_node(content_item, locale)
        result = {}
        if content_item.content_items.any?
          content_item.content_items.each do |child_item|
            result[child_item.key.to_sym] = build_hash_node(child_item, locale) if child_item.active?
          end
        else
          result = content_item.processed_value(locale)
        end
        return result
      end

      def parents
        result = resolve_parents(self)
        result.reverse
      end

      def resolve_parents content_item
        result = []
        if content_item.matestack_cms_content_item_id.present?
          node = { }
          node[:id] = content_item.matestack_cms_content_item_id
          node[:key] = ContentItem.find(content_item.matestack_cms_content_item_id).key
          result << node
          grand_parents = resolve_parents(ContentItem.find(content_item.matestack_cms_content_item_id))
          result << grand_parents unless grand_parents.empty?
        end
        return result.flatten
      end

      def all_values_as_hash
        result = {}
        ContentItem.available_locales.each do |locale|
          result[locale] = self.value_backend.read(locale)
        end
        return result
      end

      def all_values_nil?
        all_values_as_hash.values.all? { |v| v.nil? }
      end

      def all_values_present?
        all_values_as_hash.values.all? { |v| !v.nil? }
      end

      def all_values_as_string(limit=nil)
        if all_values_nil?
          return ""
        end
        result = []
        all_values_as_hash.each do |locale, value|
          result << "#{locale.to_s.upcase}: "
          result << value
          result << " "
        end
        if limit.present?
          return result.join(" ").take(limit)
        else
          return result.join(" ")
        end
      end

      def is_collection_container?
        if self.content_items.empty?
          return false
        else
          # all child keys numeric?
          # https://stackoverflow.com/questions/2095493/how-can-i-check-if-a-value-is-a-number
          self.content_items.pluck(:key).all? { |k| !(k =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/).nil? }
        end
      end

      def deep_duplicate parent_id: nil, reorder_key: true
        new_content_item = self.dup
        new_content_item.matestack_cms_content_item_id = parent_id unless parent_id.nil?

        if self.parent.is_collection_container?
          new_content_item.key = self.parent.content_items.pluck(:key).sort.last.to_i + 1 if reorder_key
        else
          new_content_item.key = self.key
        end

        new_content_item.save

        if self.asset.attached?
          new_content_item.asset.attach(self.asset.blob)
        end

        self.content_items.each do |original_child_item|
          original_child_item.deep_duplicate parent_id: new_content_item.id, reorder_key: false
        end
      end

    end
  end
end

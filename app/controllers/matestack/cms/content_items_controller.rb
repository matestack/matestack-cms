module Matestack
  module Cms
    class ContentItemsController < ApplicationController

      def create
        @content_item = Matestack::Cms::ContentItem.new(content_item_params)
        if params[:matestack_cms_content_item_id]
          @content_item.matestack_cms_content_item_id = params[:matestack_cms_content_item_id]
        end

        @content_item.save

        if @content_item.errors.any?
          render json: {errors: @content_item.errors}, status: :unprocessable_entity
        else
          I18n.backend.reload!
          render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id) }, status: :created
        end
      end

      def update
        @content_item = Matestack::Cms::ContentItem.find params[:id]

        # allow only numeric value in order to change ordering of collection items
        if content_item_params[:key].present?
          key_is_numeric = !(content_item_params[:key] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/).nil?
          if key_is_numeric
            @content_item.key = content_item_params[:key]
          end
        end

        if params[:locale_to_be_updated]
          @content_item.value_backend.write(params[:locale_to_be_updated].to_sym, content_item_params["value_#{params[:locale_to_be_updated]}".to_sym])
        else
          @content_item.value = content_item_params[:value] unless content_item_params[:value].nil?
        end

        if @content_item.save
          I18n.backend.reload!
          render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id) }, status: :created
        else
          render json: {errors: @content_item.errors}, status: :unprocessable_entity
        end
      end

      def destroy
        @content_item = Matestack::Cms::ContentItem.find params[:id]
        @parent_id = @content_item.matestack_cms_content_item_id
        if @content_item.parent.is_collection_container? && @content_item.content_items.count > 1
          if @content_item.destroy
            I18n.backend.reload!
            if @parent_id.nil?
              render json: { transition_to: Rails.application.routes.url_helpers.content_items_path }, status: :ok
            else
              render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @parent_id) }, status: :ok
            end
          else
            render json: {errors: @content_item.errors}, status: :unprocessable_entity
          end
        else
          render json: {message: "not allowed", errors: @content_item.errors}, status: :unprocessable_entity
        end
      end

      def deactivate
        @content_item = Matestack::Cms::ContentItem.find params[:id]
        @parent_id = @content_item.matestack_cms_content_item_id
        if @content_item.parent.is_collection_container?
          @content_item.active = false
          I18n.backend.reload!
          if @content_item.save
            render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id) }, status: :ok
          else
            render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id) }, status: :unprocessable_entity
          end
        else
          render json: {message: "not allowed", errors: @content_item.errors}, status: :unprocessable_entity
        end
      end

      def activate
        @content_item = Matestack::Cms::ContentItem.find params[:id]
        @parent_id = @content_item.matestack_cms_content_item_id
        if @content_item.parent.is_collection_container?
          @content_item.active = true
          I18n.backend.reload!
          if @content_item.save
            render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id) }, status: :ok
          else
            render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id) }, status: :unprocessable_entity
          end
        else
          render json: {message: "not allowed", errors: @content_item.errors}, status: :unprocessable_entity
        end
      end

      def deep_duplicate
        @content_item = Matestack::Cms::ContentItem.find params[:id]
        if @content_item.parent.is_collection_container?
          @content_item.deep_duplicate
          render json: {transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.parent.id)}, status: :ok
        else
          render json: { message: "not allowed", errors: @content_item.errors }, status: :unprocessable_entity
        end
      end

      def upload_asset
        @content_item = Matestack::Cms::ContentItem.find params[:id]
        @content_item.update(content_item_asset_params)
        if @content_item.errors.any?
          render json: {errors: @content_item.errors}, status: :unprocessable_entity
        else
          render json: {}, status: :ok
        end
      end

      def delete_asset
        @content_item = Matestack::Cms::ContentItem.find params[:id]
        begin
          @content_item.send(params[:key]).purge
          render json: { transition_to: Rails.application.routes.url_helpers.edit_content_item_path(id: @content_item.id)}, status: :ok
        rescue
          render json: {errors: @content_item.errors}, status: :unprocessable_entity
        end
      end

      def search
        render json: { transition_to: Rails.application.routes.url_helpers.search_results_content_items_path(value: search_params[:value])}, status: :ok
      end

      # def export
      #   @yml_file = Matestack::Cms::ContentItem.export(:de).to_yaml
      #   send_data @yml_file, filename: "de.yml", type: "application/yml"
      # end


      protected

      def content_item_params
        permitted_locales = Matestack::Cms::ContentItem.available_locales.map { |locale| "value_#{locale}".to_sym }
        params.require(:matestack_cms_content_item).permit(
          :key,
          *permitted_locales
        )
      end

      def content_item_asset_params
        params.permit(:asset)
      end

      def search_params
        params.require(:search).permit(:value)
      end

    end
  end
end

module Matestack
  module Cms
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception
    end
  end
end

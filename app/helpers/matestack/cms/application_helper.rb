module Matestack
  module Cms
    module ApplicationHelper

      def cms query_string
        Matestack::Cms::ContentItem.resolve_content(query_string, I18n.locale)
      end

    end
  end
end

Rails.application.routes.draw do

  root to: 'pages#some_page'

  devise_for :admins, path: 'administration', controllers: {
    sessions: 'administration/sessions'
  }

  namespace :administration do
    root to: redirect(path: 'administration/content_items')
    resources :content_items, only: [:index, :edit, :new]
  end




  # Routes for CMS Engine
  # routes for create/update/delete
  # only check for authorized admins
  authenticate :admin do
    mount Matestack::Cms::Engine => "/cms"
  end
  # make content_items path helpers available for cms engine controllers (proper redirects)
  resources :content_items, only: [:index, :edit, :new], module: 'administration', path: 'administration/content_items/' do
    collection do
      get "search_results/:value", to: 'content_items#search_results', as: "search_results"
    end
  end
end

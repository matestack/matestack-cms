# This migration comes from matestack_cms (originally 20200211143512)
class CreateMatestackContentItems < ActiveRecord::Migration[5.2]
  def change
    create_table :matestack_cms_content_items do |t|
      t.string :key
      t.belongs_to :matestack_cms_content_item, foreign_key: true, index: { name: :parent_id_on_matestack_cms_content_items }

      t.timestamps
    end
  end
end

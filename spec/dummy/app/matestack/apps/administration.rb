class Apps::Administration < Matestack::Ui::App

  def prepare
    @title = "CMS Administration"
  end

  def response
    components {
      material_top_app_bar fixed: true do
        material_top_app_bar_left do
          material_top_app_bar_navigation_button icon: 'menu'
          material_top_app_bar_title text: "ABC Admin"
        end
        material_top_app_bar_right do
          action logout_action_config do
            material_top_app_bar_action_item icon: 'exit_to_app'
          end
        end
      end
      material_drawer modal: true do
        material_drawer_header do
          heading size: 3, class: "mdc-drawer__title", text: "ABC Administration"
          heading size: 6, class: "mdc-drawer__subtitle", text: "Menü"
        end
        material_drawer_nav do

        end
      end
      div id: "page-content" do
        page_content
      end
      material_snackbar show_on: "action_success", hide_after: 3000, text: "Erfolgreich ausgeführt"
      material_snackbar show_on: "action_failure", hide_after: 10000, text: "Ein Problem ist aufgetreten"
      material_snackbar show_on: "form_success", hide_after: 3000, text: "Erfolgreich gespeichert"
      material_snackbar show_on: "form_failure", hide_after: 10000, text: "Ein Problem ist aufgetreten"
    }
  end

  protected

  def logout_action_config
    {
      method: :delete,
      path: :destroy_admin_session_path,
      success: {
        transition: {
          follow_response: true
        },
        emit: "admin_reload_page"
      }
    }
  end

end

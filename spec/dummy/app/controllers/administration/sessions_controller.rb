class Administration::SessionsController < Devise::SessionsController

  #DELETE /admin/sign_out
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    redirect_to administration_root_path, status: :see_other #https://api.rubyonrails.org/classes/ActionController/Redirecting.html
  end

end

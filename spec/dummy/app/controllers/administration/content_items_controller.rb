class Administration::ContentItemsController < ApplicationController

  layout "administration"

  include Matestack::Ui::Core::ApplicationHelper

  before_action :authenticate_admin!

  def index
    responder_for(Pages::Administration::ContentItems::Index)
  end

  def edit
    responder_for(Pages::Administration::ContentItems::Edit)
  end

  def new
    responder_for(Pages::Administration::ContentItems::New)
  end

  def search_results
    responder_for(Pages::Administration::ContentItems::SearchResults)
  end

end

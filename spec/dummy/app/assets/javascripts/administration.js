// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require matestack-ui-core
//= require matestack-ui-material
//= require matestack-cms


//Transition Start - DOM Manipulation (depends on your DOM/CSS Framework!)
MatestackUiCore.matestackEventHub.$on('page_loading', function(url){
  //hide old content
  document.querySelector('#page-content').style.opacity = 0;
});

//Transition End - DOM Manipulation (depends on your DOM/CSS Framework!)
MatestackUiCore.matestackEventHub.$on('page_loaded', function(url){
    setTimeout(function () {
      //show new content
      document.querySelector('#page-content').style.opacity = 1;
    }, 100);
});

//Transition End Scroll Top (depends on your DOM/CSS Framework!)
MatestackUiCore.matestackEventHub.$on('page_loaded', function(url){
    window.scrollTo({ top: 0 });
})

//used for transition from admin sign in to admin root view (other app) and vice versa
MatestackUiCore.matestackEventHub.$on('admin_reload_page', function(error){
  setTimeout(function () {
    location.reload()
  }, 1000);
})

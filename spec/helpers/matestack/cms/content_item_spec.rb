require 'rails_helper'

include Matestack::Cms::ApplicationHelper

describe 'Matestack::Cms::ApplicationHelper', type: :feature do

  before do
    Matestack::Cms::ContentItem.redis.keys.each do |key|
      Matestack::Cms::ContentItem.redis.del key
    end
    root = Matestack::Cms::ContentItem.create(key: 'root')
    dummy = Matestack::Cms::ContentItem.create(key: 'dummy', parent: root)
    parent = Matestack::Cms::ContentItem.create(key: 'parent', parent: dummy)
    @child = Matestack::Cms::ContentItem.create(key: 'title', parent: parent, value_en: 'Foobar')
  end

  it 'should get the correct value' do
    expect(cms('root.dummy.parent.title')).to eq 'Foobar'
  end

  it 'should get the correct hash' do
    expect(cms('root')).to eq({ dummy: { parent: { title: 'Foobar' } } })
  end

  it 'should only touch the database the first time the key is retrieved' do
    # remove key from redis cache
    Matestack::Cms::ContentItem.redis.del 'root.dummy.parent.title'
    # first call should load from db and cache the result
    expect(Matestack::Cms::ContentItem).to receive(:resolve_content_item).and_return(@child).exactly(1).times
    expect(cms('root.dummy.parent.title')).to eq 'Foobar'
    # second call should not touch the database
    expect(Matestack::Cms::ContentItem).to receive(:resolve_from_cache).exactly(1).times
    expect(cms('root.dummy.parent.title')).to eq 'Foobar'
  end

  it 'should be possible to iterate over a parent object and access child attrs via symbols as keys' do
    # when coming from cache
    cms('root').each do |child_item_key, child_item_value|
      expect(child_item_value[:parent]).to eq({ title: 'Foobar' })
    end

    Matestack::Cms::ContentItem.redis.del 'root'
    # when coming from database
    cms('root').each do |child_item_key, child_item_value|
      expect(child_item_value[:parent]).to eq({ title: 'Foobar' })
    end
  end

end

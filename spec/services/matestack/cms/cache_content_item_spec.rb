require 'rails_helper'
include Matestack::Cms::ApplicationHelper

describe 'CacheContentItem', type: :feature do

  before :each do
    Matestack::Cms::ContentItem.redis.keys.each do |key|
      Matestack::Cms::ContentItem.redis.del key
    end
  end

  it 'should cache content_item' do
    item = Matestack::Cms::ContentItem.create(key: 'title', value_en: 'Book', value_de: 'Buch', skip_cache_content_item: true)

    cached_value = Matestack::Cms::ContentItem.redis.get('en.title')
    expect(cached_value).to be nil
    
    Matestack::Cms::CacheContentItem.(item)
    cached_value = Matestack::Cms::ContentItem.redis.get('en.title')
    expect(cached_value).not_to be nil
    expect(cached_value).to eq 'Book'
  end

  it 'should cache content_item with child element' do
    parent = Matestack::Cms::ContentItem.create(key: 'some_page', skip_cache_content_item: true)
    child = Matestack::Cms::ContentItem.create(key: 'title', parent: parent, value_en: 'Book', value_de: 'Buch', skip_cache_content_item: true)

    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.some_page')
    expect(cached_value).to be nil
    
    Matestack::Cms::CacheContentItem.(parent)
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.some_page')
    expect(cached_value).not_to be nil
    expect(cached_value).to eq({title: 'Book'})
  end

  it 'should update cached parents' do
    root = Matestack::Cms::ContentItem.create(key: 'root', skip_cache_content_item: true)
    dummy = Matestack::Cms::ContentItem.create(key: 'dummy', parent: root, skip_cache_content_item: true)
    parent = Matestack::Cms::ContentItem.create(key: 'parent', parent: dummy, skip_cache_content_item: true)
    child = Matestack::Cms::ContentItem.create(key: 'title', parent: parent, value_en: 'Book', skip_cache_content_item: true)

    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root')
    expect(cached_value).to be nil
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy')
    expect(cached_value).to be nil
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy.parent')
    expect(cached_value).to be nil
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy.parent.title')
    expect(cached_value).to be nil

    Matestack::Cms::CacheContentItem.(root)
    Matestack::Cms::CacheContentItem.(parent)
    Matestack::Cms::CacheContentItem.(child)

    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root')
    expect(cached_value).to eq({ dummy: { parent: { title: 'Book' } } })
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy')
    expect(cached_value).to be nil
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy.parent')
    expect(cached_value).to eq({ title: 'Book' })
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy.parent.title')
    expect(cached_value).to eq 'Book'

    child.value_en = 'Literatur'
    child.save
    Matestack::Cms::CacheContentItem.(child)

    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy.parent.title')
    expect(cached_value).to eq 'Literatur'
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy.parent')
    expect(cached_value).to eq({ title: 'Literatur' })
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root.dummy')
    expect(cached_value).to be nil
    cached_value = parse Matestack::Cms::ContentItem.redis.get('en.root')
    expect(cached_value).to eq({ dummy: { parent: { title: 'Literatur' } } })
  end

end

private 

def parse(string)
  return unless string
  (json = JSON.parse(string) rescue string).is_a?(Hash) ? json.deep_symbolize_keys : json
end
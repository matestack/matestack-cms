Matestack::Cms::Engine.routes.draw do

  resources :content_items, only: [:edit, :update, :create, :destroy] do
    member do
      post 'upload_asset', to: 'content_items#upload_asset'
      delete 'asset/:key', to: 'content_items#delete_asset', as: 'delete_asset'
      post 'deep_duplicate', to: 'content_items#deep_duplicate', as: 'deep_duplicate'
      put 'deactivate', to: 'content_items#deactivate', as: 'deactivate'
      put 'activate', to: 'content_items#activate', as: 'activate'
    end
    collection do
      # get 'export', to: 'content_items#export'
      post 'search', to: 'content_items#search'
    end
  end

end
